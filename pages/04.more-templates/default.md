---
title: 'More Templates'
sidebarlayout: sidebar-top-showcase
sidebarpath: /sidebar
showcase: true
hidetoc: false
horizontaltoc: true
---

This is the default template with the sidebar and a showcase at the top.

You will see a random photo of grilled cheese every time you refresh.

## Images used

[Simple grilled cheese](https://commons.wikimedia.org/wiki/File:Simple_grilled_cheese.jpg) by Mack Male from Edmonton, AB, Canada  
[Grilled cheese sandwich](https://commons.wikimedia.org/wiki/File:Grilled_cheese_sandwich.jpg) by Maggie Hoffman  
[Grilled cheese with pepperoni](https://commons.wikimedia.org/wiki/File:Grilled_cheese_with_pepperoni.jpg) by Michael Martine from Montpelier, USA  
[Grilled cheese sandwich with tomato soup](https://commons.wikimedia.org/wiki/File:Grilled_cheese_sandwich_with_tomato_soup.jpg) by Arnold Gatilao from Oakland, CA, USA