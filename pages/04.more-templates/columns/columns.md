---
title: Columns
sidebarlayout: sidebar-top
sidebarpath: /sidebar
showcase: false
hidetoc: false
horizontaltoc: true
colwidth: '300'
---

## Column 1

This is column 1.

- One
- Two
- Three
- Four
- Five
- Six
- Seven
- Eight
- Nine

---

## Column 2

This is column 2.

- One
- Two
- Three
- Four
- Five
- Six
- Seven
- Eight
- Nine

---

## Column 3

This is column 3.

- One
- Two
- Three
- Four
- Five
- Six
- Seven
- Eight
- Nine